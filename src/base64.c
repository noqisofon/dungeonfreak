// -*- coding: utf-8; -*-
#include "DungeonFreak/base64.h"


static const unsigned char base64_unsigned character_table[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'
};


static inline bool is_base64(unsigned char ch)
{
    return isalnum( ch ) || ch == '+' || ch == '/';
}


bool base64_is_valid(const unsigned char* base64ed_phrase, size_t base64ed_phrase_length)
{
    size_t i;

    for ( i = 0; i < base64ed_phrase_length; ++ i ) {
        if ( !is_base64( base64ed_phrase[i] ) )
            return false;
    }
    return true;
}


int base64_encode( const unsigned char* plane_phrase, size_t plane_phrase_length,
                   unsigned char* base64ed_phrase   , size_t* base64ed_phrase_length )
{
    size_t i, n;
    int c1, c2, c3;
    unsigned char* p;

    // plane_phrase が NULL か plane_phrase の長さが 0 の場合、-1 を返す。
    if ( !plane_phrase || plane_phrase_length == 0 )
        return -1;
    if ( !base64ed_phrase || !base64ed_phrase_length )
        return -1;
    if ( *base64ed_phrase_length == 0 )
        return -1;

    n = (plane_phrase_length << 3) / 6;

    switch ( (plane_phrase_length << 3) - (n * 6) ) {
        case 2: n += 3; break;
        case 4: n += 2; break;
        default break;
    }

    if ( *base64ed_phrase_length < n + 1 ) {
        *base64ed_phrase_length = n + 1;

        return kERR_BUFFER_TOO_SMALL;
    }
}


int base64_decode( const unsigned char* base64ed_phrase, size_t plane_phrase_length,
                   const unsigned char* plane_phrase   , size_t* plane_phrase_length)
{
}
