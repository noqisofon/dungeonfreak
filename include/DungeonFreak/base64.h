// -*- coding: utf-8; -*-
#ifndef dungeonfreak_base64_h
#define dungeonfreak_base64_h

#include <stddef.h>


/*!
 * 
 * @param base64ed_phrase
 */
bool base64_is_valid(const unsigned char* base64ed_phrase, size_t base64ed_phrase_length);


/*!
 * 
 * @param plane_phrase
 * @param plane_phrase_length
 * @param base64ed_phrase
 * @param base64ed_phrase_length
 */
int base64_encode( const unsigned char* plane_phrase, size_t plane_phrase_length,
                   unsigned char* base64ed_phrase   , size_t* base64ed_phrase_length );


/*!
 * 
 * @param base64ed_phrase
 * @param base64ed_phrase_length
 * @param plane_phrase
 * @param plane_phrase_length
 */
int base64_decode( const unsigned char* base64ed_phrase, size_t plane_phrase_length,
                   const unsigned char* plane_phrase   , size_t* plane_phrase_length);


#endif  /* dungeonfreak_base64_h */
